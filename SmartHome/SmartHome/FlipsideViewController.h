//
//  FlipsideViewController.h
//  SmartHome
//
//  Created by Olivan Aires on 18/05/14.
//
//

#import <UIKit/UIKit.h>
#import "BSKeyboardControls.h"

@class FlipsideViewController;

@protocol FlipsideViewControllerDelegate
- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller;
@end

@interface FlipsideViewController : UIViewController <UITextFieldDelegate, UITextViewDelegate, BSKeyboardControlsDelegate>

@property (weak, nonatomic) id <FlipsideViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITextField *enderecoIP;
@property (weak, nonatomic) IBOutlet UITextField *porta;
@property (weak, nonatomic) IBOutlet UITextField *servidorDNS;

- (IBAction)salvarConfiguracao:(id)sender;

@end
