//
//  FlipsideViewController.m
//  SmartHome
//
//  Created by Olivan Aires on 18/05/14.
//
//

#import "FlipsideViewController.h"
#import "BSKeyboardControls.h"

#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

enum
{
    kSectionDeveloper = 4
};

enum
{
    kRowDeveloper = 0
};

@interface FlipsideViewController ()
- (IBAction)salvarConfiguracao:(id)sender;
@property (nonatomic, strong) BSKeyboardControls *keyboardControls;
@end

@implementation FlipsideViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    NSMutableDictionary *dictionary = [self getShopFile];
    NSString *validaEnderecoIp = [dictionary objectForKey:@"enderecoIp"];
    NSString *validaPorta = [dictionary objectForKey:@"porta"];
    NSString *validaServidorDNS = [dictionary objectForKey:@"servidorDNS"];

    if ([validaEnderecoIp length] != 0 && [validaPorta length] != 0) {
        self.enderecoIP.text = validaEnderecoIp;
        self.porta.text = validaPorta;
        self.servidorDNS.text = validaServidorDNS;
    }

    NSArray *fields = @[self.enderecoIP, self.porta, self.servidorDNS];
    [self setKeyboardControls:[[BSKeyboardControls alloc] initWithFields:fields]];
    [self.keyboardControls setKeyBoardDelegate:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSMutableDictionary*)getShopFile {
    NSString *plistPath = [self getPlistDirectory:@"Configuracao-App"];
    NSMutableDictionary *plistDictionary = [NSMutableDictionary dictionaryWithContentsOfFile:plistPath];
    return plistDictionary;
}

-(NSString*)getPlistDirectory:(NSString*)aFilename {
    NSString *plistPath;
    NSString *fullFileName = [aFilename stringByAppendingString:@".plist"];
    NSError *error;
    NSString *documentDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    plistPath = [documentDirectory stringByAppendingPathComponent:fullFileName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath: plistPath]) {
        NSString *bundle = [[NSBundle mainBundle] pathForResource:aFilename ofType:@"plist"];
        [fileManager copyItemAtPath:bundle toPath:plistPath error:&error];
    }
    return plistPath;
}

- (IBAction)salvarConfiguracao:(id)sender {
    NSMutableDictionary *dictionary = [self getShopFile];
    [dictionary setValue:[NSString stringWithFormat:@"%@", self.enderecoIP.text] forKey:@"enderecoIp"];
    [dictionary setValue:[NSString stringWithFormat:@"%@", self.porta.text] forKey:@"porta"];
    [dictionary setValue:[NSString stringWithFormat:@"%@", self.servidorDNS.text] forKey:@"servidorDNS"];
    [dictionary writeToFile:[self getPlistDirectory:@"Configuracao-App"] atomically:YES];
    [self.delegate flipsideViewControllerDidFinish:self];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad || toInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark -
#pragma mark Text Field Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.keyboardControls setActiveField:textField];
}

#pragma mark -
#pragma mark Text View Delegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [self.keyboardControls setActiveField:textView];
}

#pragma mark -
#pragma mark Keyboard Controls Delegate

- (void)keyboardControls:(BSKeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(BSKeyboardControlsDirection)direction
{
    UIView *view;

    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        view = field.superview.superview;
    } else {
        view = field.superview.superview.superview;
    }
}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:YES];
}

@end
