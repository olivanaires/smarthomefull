//
//  AppDelegate.h
//  SmartHome
//
//  Created by Olivan Aires on 19/05/14.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
