//
//  ViewController.m
//  SmartHome
//
//  Created by Olivan Aires on 14/05/14.
//
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize pinDig01;
@synthesize pinDig02;
@synthesize pinDig03;
@synthesize pinDig05;
@synthesize pinDig06;
@synthesize pinDig07;
@synthesize pinDig08;
@synthesize pinDig09;
@synthesize pinDig00;
@synthesize pinAnal00;
@synthesize pinAnal01;
@synthesize pinAnal02;
@synthesize pinAnal03;
@synthesize pinAnal04;
@synthesize pinAnal05;
@synthesize labelAnal00;
@synthesize labelAnal01;
@synthesize labelAnal02;
@synthesize labelAnal03;
@synthesize labelAnal04;
@synthesize labelAnal05;
@synthesize theURL;
@synthesize url;

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)atualizarPinosDigitais:(NSDictionary *)retornoJSON
{
    //atualiza o valor dos switch referente as portas digitais
    pinDig01.on = [[retornoJSON objectForKey:@"valuePinDig01"] intValue] == 1;
    pinDig02.on = [[retornoJSON objectForKey:@"valuePinDig02"] intValue] == 1;
    pinDig03.on = [[retornoJSON objectForKey:@"valuePinDig03"] intValue] == 1;
    pinDig05.on = [[retornoJSON objectForKey:@"valuePinDig05"] intValue] == 1;
    pinDig06.on = [[retornoJSON objectForKey:@"valuePinDig06"] intValue] == 1;
    pinDig07.on = [[retornoJSON objectForKey:@"valuePinDig07"] intValue] == 1;
    pinDig08.on = [[retornoJSON objectForKey:@"valuePinDig08"] intValue] == 1;
    pinDig09.on = [[retornoJSON objectForKey:@"valuePinDig09"] intValue] == 1;
    pinDig00.on = [[retornoJSON objectForKey:@"valuePinDig00"] intValue] == 1;
}

- (void)atualizarPinosAnalogicos:(NSDictionary *)retornoJSON
{
    //atualiza o valor dos campos texto referente as portas analogicas
    [pinAnal00 setText:[NSString stringWithString:[retornoJSON objectForKey:@"valuePinAnal00"]]];
    [pinAnal01 setText:[NSString stringWithString:[retornoJSON objectForKey:@"valuePinAnal01"]]];
    [pinAnal02 setText:[NSString stringWithString:[retornoJSON objectForKey:@"valuePinAnal02"]]];
    [pinAnal03 setText:[NSString stringWithString:[retornoJSON objectForKey:@"valuePinAnal03"]]];
    [pinAnal04 setText:[NSString stringWithString:[retornoJSON objectForKey:@"valuePinAnal04"]]];
    [pinAnal05 setText:[NSString stringWithString:[retornoJSON objectForKey:@"valuePinAnal05"]]];
}

- (IBAction)atualizar:(id)sender {
    NSError* error;
    NSData *jsonData;
    NSMutableDictionary *dictionary = [self getShopFile];
    NSString *validaServidorDNS = [dictionary objectForKey:@"servidorDNS"];

    if (validaServidorDNS != nil) {
        theURL = [NSString stringWithFormat:@"%@",validaServidorDNS];
        url = [NSURL URLWithString:theURL];
        jsonData = [NSData dataWithContentsOfURL:url];
        if (jsonData != nil) {
            NSDictionary *resultados = [NSJSONSerialization JSONObjectWithData:jsonData
                                                                       options:NSJSONReadingMutableContainers error:&error];
            if(!error) {
                [self atualizarPinosAnalogicos:resultados];
            } else {
                NSLog(@"Encontrado erro no arquivo JSON !");
            }
        } else {
            NSLog(@"Não foi possivel se conectar!");
        }
    }else {
        NSLog(@"Não existe endereço cadastrado!");
    }
}

-(NSMutableDictionary*)getShopFile {
    NSString *plistPath = [self getPlistDirectory:@"Configuracao-App"];
    NSMutableDictionary *plistDictionary = [NSMutableDictionary dictionaryWithContentsOfFile:plistPath];
    return plistDictionary;
}

-(NSString*)getPlistDirectory:(NSString*)aFilename {
    NSString *plistPath;
    NSString *fullFileName = [aFilename stringByAppendingString:@".plist"];
    NSError *error;
    NSString *documentDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    plistPath = [documentDirectory stringByAppendingPathComponent:fullFileName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath: plistPath]) {
        NSString *bundle = [[NSBundle mainBundle] pathForResource:aFilename ofType:@"plist"];
        [fileManager copyItemAtPath:bundle toPath:plistPath error:&error];
    }
    return plistPath;
}

- (IBAction)changePinDig:(id)sender {
    UISwitch *switchAtual = (UISwitch *) sender;
    NSError *error;
    NSMutableDictionary *dictionary = [self getShopFile];

    NSString *validaServidorDNS = [dictionary objectForKey:@"servidorDNS"];

    if (validaServidorDNS != nil) {


        if (switchAtual.isOn) {
            theURL = [NSString stringWithFormat:@"%@/%@Ativar",validaServidorDNS,switchAtual.accessibilityLabel];
            url = [NSURL URLWithString:theURL];
            NSData *jsonData = [NSData dataWithContentsOfURL:url ];
            if (jsonData != nil) {
                NSDictionary *resultados = [NSJSONSerialization JSONObjectWithData:jsonData
                                                                           options:NSJSONReadingMutableContainers error:&error];
                if(!error) {
                    [self atualizarPinosDigitais:resultados];
                } else {
                    switchAtual.on = FALSE;
                    NSLog(@"Encontrado erro no arquivo JSON !");
                }
            } else {
                switchAtual.on = FALSE;
                NSLog(@"Não foi possivel se conectar!");
            }
        }
        else {
            theURL = [NSString stringWithFormat:@"%@/%@Desativar",validaServidorDNS,switchAtual.accessibilityLabel];
            url = [NSURL URLWithString:theURL];
            NSData *jsonData = [NSData dataWithContentsOfURL:url];
            if (jsonData != nil) {
                NSDictionary *resultados = [NSJSONSerialization JSONObjectWithData:jsonData
                                                                           options:NSJSONReadingMutableContainers error:&error];
                if(!error) {
                    [self atualizarPinosDigitais:resultados];
                } else {
                    switchAtual.on = TRUE;
                    NSLog(@"Encontrado erro no arquivo JSON !");
                }
            } else {
                switchAtual.on = TRUE;
                NSLog(@"Não foi possivel se conectar!");
            }

        }

    }else {
        switchAtual.on = TRUE;
        NSLog(@"Não existe endereço cadastrado!");
    }
}

- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showAlternate"]) {
        [[segue destinationViewController] setDelegate:self];
    }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self orientacaoMudou];
}

- (void)orientacaoMudou
{
    UIInterfaceOrientation orientacao = self.interfaceOrientation;
    if (orientacao == UIInterfaceOrientationLandscapeLeft || orientacao == UIInterfaceOrientationLandscapeRight) {
        labelAnal00.frame = CGRectMake(308, 106, 78, 21);
        pinAnal00.frame = CGRectMake(385, 106, 39, 21);
        labelAnal03.frame = CGRectMake(431, 106, 78, 21);
        pinAnal03.frame = CGRectMake(509, 106, 39, 21);
        labelAnal01.frame = CGRectMake(308, 142, 78, 21);
        pinAnal01.frame = CGRectMake(385, 142, 39, 21);
        labelAnal04.frame = CGRectMake(431, 142, 78, 21);
        pinAnal04.frame = CGRectMake(509, 142, 39, 21);
        labelAnal02.frame = CGRectMake(308, 176, 78, 21);
        pinAnal02.frame = CGRectMake(385, 176, 39, 21);
        labelAnal05.frame = CGRectMake(431, 176, 78, 21);
        pinAnal05.frame = CGRectMake(509, 176, 39, 21);
    }
}

@end
