//
//  ViewController.h
//  SmartHome
//
//  Created by Olivan Aires on 14/05/14.
//
//

#import "FlipsideViewController.h"

@interface ViewController : UIViewController <FlipsideViewControllerDelegate>

@property (strong, nonatomic) NSString *theURL;
@property (strong, nonatomic) NSURL *url;
@property (weak, nonatomic) IBOutlet UISwitch *pinDig00;
@property (weak, nonatomic) IBOutlet UISwitch *pinDig01;
@property (weak, nonatomic) IBOutlet UISwitch *pinDig02;
@property (weak, nonatomic) IBOutlet UISwitch *pinDig03;
@property (weak, nonatomic) IBOutlet UISwitch *pinDig05;
@property (weak, nonatomic) IBOutlet UISwitch *pinDig06;
@property (weak, nonatomic) IBOutlet UISwitch *pinDig07;
@property (weak, nonatomic) IBOutlet UISwitch *pinDig08;
@property (weak, nonatomic) IBOutlet UISwitch *pinDig09;

@property (weak, nonatomic) IBOutlet UILabel *pinAnal00;
@property (weak, nonatomic) IBOutlet UILabel *pinAnal01;
@property (weak, nonatomic) IBOutlet UILabel *pinAnal02;
@property (weak, nonatomic) IBOutlet UILabel *pinAnal03;
@property (weak, nonatomic) IBOutlet UILabel *pinAnal04;
@property (weak, nonatomic) IBOutlet UILabel *pinAnal05;
@property (weak, nonatomic) IBOutlet UILabel *labelAnal00;
@property (weak, nonatomic) IBOutlet UILabel *labelAnal01;
@property (weak, nonatomic) IBOutlet UILabel *labelAnal02;
@property (weak, nonatomic) IBOutlet UILabel *labelAnal03;
@property (weak, nonatomic) IBOutlet UILabel *labelAnal04;
@property (weak, nonatomic) IBOutlet UILabel *labelAnal05;

@end
