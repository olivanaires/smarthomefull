#include <SPI.h>
#include <Ethernet.h>

byte mac[] = { 
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};

IPAddress ip(192,168,0,177);

EthernetClient client;

char server[] = "192.168.0.28";

unsigned long lastConnectionTime = 0;             // last time you connected to the server, in milliseconds
boolean lastConnected = false;                    // state of the connection last time through the main loop
const unsigned long postingInterval = 60L*1000L;  // delay between updates, in milliseconds
						  // the "L" is needed to use long type numbers

void setup() {
  Serial.begin(9600);
  delay(1000);
  Ethernet.begin(mac, ip);
  Serial.println(Ethernet.localIP());
}

void loop() {
  if (client.available()) {
    char c = client.read();
    Serial.print(c);
  }

  if (!client.connected() && lastConnected) {
    Serial.println();
    Serial.println("disconnecting.");
    client.stop();
  }

  if(!client.connected() && (millis() - lastConnectionTime > postingInterval)) {
    httpRequest("Teste, por favor da certo!");
  }
  lastConnected = client.connected();
}

// this method makes a HTTP connection to the server:
void httpRequest(String msg) {
  // if there's a successful connection:
  String body = HttpRequestBody(msg);
  if (client.connect(server, 8080)) {
    Serial.println("conectando...");

    client.println("POST /JaxWsSample/PushNotificationServiceImpl HTTP/1.1");
    client.println("Accept-Encoding: gzip,deflate");
    client.println("Content-Type: text/xml;charset=UTF-8");
    client.println("SOAPAction: \"\"");
    client.print("Content-Length: ");
    client.println(body.length());
    client.println("Host: 192.168.0.28:8080"); 
    client.println("Connection: Keep-Alive");
    client.println("User-Agent: Arduino Ethernet");
    client.println();
    client.println(body);
    client.println();
    client.stop();
    Serial.println("Notificacao enviada!");

    // note the time that the connection was made:
    lastConnectionTime = millis();
  } 
  else {
    // if you couldn't make a connection:
    Serial.println("connection failed");
    Serial.println("disconnecting.");
    client.stop();
  }
}

String HttpRequestBody(String msg) {
  String res = "";
  res +="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:impl=\"http://impl.service.br.com/\">[\n]";
  res +="   <soapenv:Header/>[\n]";
  res +="   <soapenv:Body>[\n]";
  res +="      <impl:sendPushNotification>[\n]";
  res +="         <msg>"+msg+"</msg>[\n]";
  res +="      </impl:sendPushNotification>[\n]";
  res +="   </soapenv:Body>[\n]";
  res +="</soapenv:Envelope>";
  return  res;
}




