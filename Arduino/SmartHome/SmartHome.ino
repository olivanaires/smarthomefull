#include <SPI.h>
#include <Ethernet.h>
#include <IRremote.h>
#include <avr/pgmspace.h>

byte mac[] = { 
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress ip(192,168,0,177);
EthernetServer server(7071);

PROGMEM prog_uint16_t ligarAr[200] = {4500,4450,600,1650,550,550,600,1600,600,1600,550,550,600,500,600,1650,550,550,600,500,600,1600,600,500,600,500,600,1600,600,1650,550,550,600,1600,550,550,600,500,600,500,600,1650,550,1650,550,1650,600,1600,550,1650,600,1650,550,1650,550,1650,600,500,600,500,600,500,600,500,600,500,600,550,550,550,550,550,600,500,600,1600,550,550,600,500,600,500,600,1650,550,1650,550,1650,600,1600,600,500,600,1650,550,1650,600,1600,550,5350,4550,4450,600,1600,550,550,600,1600,600,1650,550,550,550,550,600,1600,600,500,600,500,600,1600,600,550,550,550,550,1650,600,1600,600,500,600,1600,600,500,600,550,550,550,550,1650,550,1650,600,1600,600,1650,550,1650,600,1600,550,1650,600,1600,600,500,600,500,600,550,550,550,550,550,600,500,600,500,600,500,600,500,600,1650,550,500,600,550,550,550,550,1650,550,1650,600,1600,600,1650,550,550,550,1650,550,1650,550,1650,600,};
PROGMEM prog_uint16_t desligarAr[200] = {4550,4450,600,1600,600,500,600,1650,550,1650,550,550,600,500,600,1600,600,500,600,550,550,1650,550,550,600,500,600,1600,550,1650,600,500,600,1650,550,550,600,1600,600,1600,550,1650,600,1650,550,550,550,1650,600,1600,550,1650,600,500,600,550,550,550,550,550,600,1600,550,550,600,500,600,1600,600,1650,550,1650,550,550,600,500,600,500,600,500,600,500,600,500,600,500,600,550,550,1650,550,1650,600,1600,600,1600,600,1650,550,5350,4500,4500,550,1650,550,550,600,1600,600,1600,550,550,600,500,600,1650,550,550,600,500,600,1600,600,500,600,500,600,1650,550,1650,550,550,600,1600,600,500,600,1600,600,1650,550,1650,550,1650,600,500,600,1600,600,1600,600,1650,550,550,550,550,600,500,600,500,600,1600,600,500,600,500,600,1650,550,1650,600,1600,550,550,600,500,600,500,600,500,600,550,550,550,550,550,600,500,600,1600,600,1600,600,1650,550,1600,650,1600,550,};
PROGMEM prog_uint16_t onOffTV[136] = {4550,4500,550,1700,550,1700,500,1700,550,600,550,550,600,550,550,550,550,600,550,1650,550,1700,550,1700,550,550,600,550,550,550,550,600,550,550,550,550,600,1650,550,600,550,550,550,550,600,550,550,550,550,600,550,1650,550,600,550,1700,500,1700,550,1700,550,1700,550,1700,550,1700,500,-18686,4550,4500,550,1700,500,1700,550,1700,550,550,600,550,550,550,600,550,550,550,550,1700,550,1700,550,1700,500,600,550,550,600,550,550,550,550,550,600,550,550,1700,500,600,550,600,550,550,550,550,600,550,550,550,550,1700,550,550,600,1650,550,1700,550,1700,550,1700,500,1700,550,1700,550,};
unsigned int sinalAr[200];

IRsend irsend;

void setup() {
  Serial.begin(9600);
  Ethernet.begin(mac, ip);
  server.begin();
  pinMode(1, OUTPUT); 
  pinMode(2, OUTPUT); 
  pinMode(5, OUTPUT); 
  pinMode(6, OUTPUT); 
  pinMode(7, OUTPUT); 
  pinMode(8, OUTPUT); 
  pinMode(9, OUTPUT); 
}


void loop() {

  // listen for incoming clients
  EthernetClient client = server.available();
  if (client) {
    Serial.println("Conectado!");
    // an http request ends with a blank line
    boolean currentLineIsBlank = true;
    String vars;
    int varOnOff = 0;
    int varPorta=9999;
    boolean passou = false;

    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        Serial.write(c);
        vars.concat(c);

        if ( vars.indexOf("Ativar") > 0 && passou == false ) {
          varOnOff = 1;
          passou = true;
          varPorta = vars.substring( vars.length()-7, vars.length()-6 ).toInt();
        } else if ( vars.indexOf("Desativar") > 0 && passou == false ) {
          varOnOff = 2;
          passou = true;
          varPorta = vars.substring( vars.length()-10, vars.length()-9 ).toInt();
        } else if ( vars.indexOf("LigarAr") > 0 && passou == false ) {
          varOnOff=3;
          passou = true;
        } else if ( vars.indexOf("DesligarAr") > 0 && passou == false ) {
          varOnOff=4;
          passou = true;
        }
        
        if (c == '\n' && currentLineIsBlank) {
          if ( varOnOff == 1 && passou == true) {
            digitalWrite(varPorta, HIGH);
          } else if ( varOnOff == 2 && passou == true ) {
            digitalWrite(varPorta, LOW);
          } else if ( varOnOff == 3 && passou == true ) {
            setarSinalAr(ligarAr);
            irsend.sendRaw(sinalAr,200,38);
          } else if ( varOnOff == 4 && passou == true ) {
            setarSinalAr(desligarAr);
            irsend.sendRaw(sinalAr,200,38);
          }
          // send a standard http response header
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: application/json;charset=utf-8");
          client.println("Server: Arduino");
          client.println("Connnection: close");
          client.println();
          client.print("{\"valuePinDig01\":\"");
          client.print(digitalRead(1));
          client.print("\",");
          client.print("\"valuePinDig02\":\"");
          client.print(digitalRead(2));
          client.print("\",");
          client.print("\"valuePinDig05\":\"");
          client.print(digitalRead(5));
          client.print("\",");
          client.print("\"valuePinDig06\":\"");
          client.print(digitalRead(6));
          client.print("\",");
          client.print("\"valuePinDig07\":\"");
          client.print(digitalRead(7));
          client.print("\",");
          client.print("\"valuePinDig08\":\"");
          client.print(digitalRead(8));
          client.print("\",");
          client.print("\"valuePinDig09\":\"");
          client.print(digitalRead(9));
          client.print("\",");
          client.print("\"valuePinAnal00\":\"");
          client.print(analogRead(0));
          client.print("\",");
          client.print("\"valuePinAnal01\":\"");
          client.print(analogRead(1));
          client.print("\",");
          client.print("\"valuePinAnal02\":\"");
          client.print(analogRead(2));
          client.print("\"}");
          client.println();
          break;
        }
        if (c == '\n') {
          // you're starting a new line
          currentLineIsBlank = true;
        } 
        else if (c != '\r') {
          // you've gotten a character on the current line
          currentLineIsBlank = false;
        }
      }
    }
    // give the web browser time to receive the data
    delay(1);
    // close the connection:
    client.stop();
    Serial.println("client disconnected");
  }
}


void setarSinalAr(prog_uint16_t* sinal){
  int k;
  for(k=0;k<199;k++){
    sinalAr[k]=pgm_read_word_near(sinal + k);
  }
}

