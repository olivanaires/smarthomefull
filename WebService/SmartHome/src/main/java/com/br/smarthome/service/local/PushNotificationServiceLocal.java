package com.br.smarthome.service.local;


public interface PushNotificationServiceLocal {
	
	public void sendPushNotification( String msg);

}
