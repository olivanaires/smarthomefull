package com.br.smarthome.service.impl;

import java.util.List;

import javapns.Push;
import javapns.communication.exceptions.CommunicationException;
import javapns.communication.exceptions.KeystoreException;
import javapns.notification.PushNotificationPayload;
import javapns.notification.PushedNotification;
import javapns.notification.ResponsePacket;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.apache.log4j.BasicConfigurator;
import org.json.JSONException;

import com.br.smarthome.service.local.PushNotificationServiceLocal;

@WebService
public class PushNotificationServiceImpl implements PushNotificationServiceLocal {

	@Override
	@WebMethod
	public void sendPushNotification( @WebParam( name = "msg" )
	String msg ) {
		BasicConfigurator.configure();
		try {
			PushNotificationPayload notificacao = PushNotificationPayload.complex();
			notificacao.addAlert( msg );
			notificacao.addBadge( 1 );
			notificacao.addSound( "default" );
			notificacao.addCustomDictionary( "id", "1" );
			System.out.println( notificacao.toString() );
			List<PushedNotification> NOTIFICATIONS = Push.payload( notificacao, "ck.p12", "O22t76a85%", false, "98ae6bb2aeaba99900c80af367462b50dcba05d2dea3b4526089e77830cbeb66" );

			for ( PushedNotification NOTIFICATION : NOTIFICATIONS ) {
				if ( NOTIFICATION.isSuccessful() ) {
					System.out.println( "Notificação enviada com sucesso para: " + NOTIFICATION.getDevice().getToken() );
				} else {
					Exception THEPROBLEM = NOTIFICATION.getException();
					THEPROBLEM.printStackTrace();
					ResponsePacket THEERRORRESPONSE = NOTIFICATION.getResponse();
					if ( THEERRORRESPONSE != null ) {
						System.out.println( THEERRORRESPONSE.getMessage() );
					}
				}
			}

		} catch ( CommunicationException e ) {
			e.printStackTrace();
		} catch ( KeystoreException e ) {
			e.printStackTrace();
		} catch ( JSONException e ) {
			e.printStackTrace();
		}
	}
}
