package com.br.service.local;


public interface PushNotificationServiceLocal {
	
	public void sendPushNotification( String msg);

}
