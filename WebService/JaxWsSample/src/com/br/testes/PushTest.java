package com.br.testes;

import javapns.Push;
import javapns.communication.exceptions.CommunicationException;
import javapns.communication.exceptions.KeystoreException;

public class PushTest {
	
	public static void main( String[] args ) {
		try {
			Push.alert( "Mensagem Teste", "files/ck.p12", "O22t76a85%", false, "98ae6bb2aeaba99900c80af367462b50dcba05d2dea3b4526089e77830cbeb66" );
		} catch ( CommunicationException | KeystoreException e ) {
			e.printStackTrace();
		}
	}
}
